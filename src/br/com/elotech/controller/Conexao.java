package br.com.elotech.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.swing.JOptionPane;
import Services.CriaSchema;

public class Conexao {

	Connection con = null;
	
	private String url, usuario, senha;
	private String pastaPostgres;
	
	public Conexao() {
	}
	
	public Conexao(String pastaPostgres) {
		this.pastaPostgres = pastaPostgres;
	}

	public Conexao(String coletaipBanco, String coletaUsuario, String coletaSenha, String coletaCaminhoBanco,
			String NomeDataBase, CriaSchema cria) throws SQLException {

		this.url = "jdbc:postgresql://" + coletaipBanco + ":5432/template1";
		this.usuario = coletaUsuario.toLowerCase();
		this.senha = coletaSenha.toLowerCase();

		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection(url, usuario, senha);
			Statement st = con.createStatement();
			st.executeUpdate("CREATE DATABASE \"" + NomeDataBase.toUpperCase() + "\" WITH OWNER = 'aise' ENCODING = "
					+ "'WIN1252' TABLESPACE = 'pg_default' LC_COLLATE = 'Portuguese_Brazil.1252' "
					+ "LC_CTYPE = 'Portuguese_Brazil.1252' CONNECTION LIMIT = -1 TEMPLATE = 'template0';");


			this.url = "jdbc:postgresql://" + coletaipBanco + ":5432/" + NomeDataBase.toUpperCase();
			this.usuario = coletaUsuario.toLowerCase();
			this.senha = coletaSenha.toLowerCase();
			con = DriverManager.getConnection(url, usuario, senha);

			if ((cria.getAdcionaSchemaAise() != "")) {
				st = con.createStatement();
				st.executeUpdate("CREATE SCHEMA aise;");
				st.executeUpdate("ALTER SCHEMA aise OWNER TO aise;");
			}
			if (cria.getAdcionaSchemaApice() != "") {
				st = con.createStatement();
				st.executeUpdate("CREATE SCHEMA apice;");
				st.executeUpdate("ALTER SCHEMA apice OWNER TO apice;");
			}
			if (cria.getAdcionaSchemaEloArquivo() != "") {
				st = con.createStatement();
				st.executeUpdate("CREATE SCHEMA eloarquivo;");
				st.executeUpdate("ALTER SCHEMA aise OWNER TO eloarquivo;");
			}
			if (cria.getAdcionaSchemaProtocolo() != "") {
				st = con.createStatement();
				st.executeUpdate("CREATE SCHEMA protocolo;");
				st.executeUpdate("ALTER SCHEMA aise OWNER TO protocolo;");
			}
			if (cria.getAdcionaSchemaSigeloam() != "") {
				st = con.createStatement();
				st.executeUpdate("CREATE SCHEMA sigeloam;");
				st.executeUpdate("ALTER SCHEMA aise OWNER TO sigeloam;");
			}
			if (cria.getAdcionaSchemaSigelorj() != "") {
				st = con.createStatement();
				st.executeUpdate("CREATE SCHEMA sigelorj;");
				st.executeUpdate("ALTER SCHEMA aise OWNER TO sigelorj;");
			}
			if (cria.getAdcionaSchemaSiscop() != "") {
				st = con.createStatement();
				st.executeUpdate("CREATE SCHEMA siscop;");
				st.executeUpdate("ALTER SCHEMA aise OWNER TO siscop;");
			}			
			if (cria.getAdcionaSchemaSiscop() != "") {
				st = con.createStatement();
				st.executeUpdate("CREATE SCHEMA unico;");
				st.executeUpdate("ALTER SCHEMA aise OWNER TO unico;");
			}

			
			JOptionPane.showMessageDialog(null,
					"Conex�o com banco dados e DataBase\n" + NomeDataBase + "\nCriada com Sucesso!!");
			
			
		} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Usuario ou Senha Incorreta");
				JOptionPane.showMessageDialog(null, e);
				con.rollback();
			
		} finally {
			con.close();
		}
	}
	
	public void testarConexao(String ipBanco, String usuario, String senha) {
		this.url = "jdbc:postgresql://" + ipBanco + ":5432/template1";
		this.usuario = usuario.toLowerCase();
		this.senha = senha.toLowerCase();
		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection(this.url, usuario, senha);
			Statement st = con.createStatement();
			ResultSet result = st.executeQuery("select current_timestamp;");
			result.next();
			JOptionPane.showMessageDialog(null, "Conectado OK " + result.getString(1));
		} catch (SQLException e) {			
			JOptionPane.showMessageDialog(null, e.toString() + "Erro de Conex�o");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createBackupCommand(String usuario, String senha, String server, String databaseName, 
			String schemaList, String Caminho) {
		String path = System.getProperty("user.dir");
		String localDoArquivo = path + "\\backup.bat";
		FileWriter arq;
		try {
			arq = new FileWriter(localDoArquivo);
			PrintWriter gravarArq = new PrintWriter(arq);

			JOptionPane.showMessageDialog(null, "Foi gerado um arquivo .bat no Seguinte Caminho " + localDoArquivo
					+ " \nA restauracao irah iniciar em instantes");

			gravarArq.printf("set pguser=" + usuario.toLowerCase() + "\nset pgpassword=" + senha.toLowerCase() + "\n");
			gravarArq.printf(pastaPostgres + "pg_dump.exe" + " -h " + server + " -p 5432 -U " + usuario.toLowerCase());
			gravarArq.printf(schemaList);
			gravarArq.printf("-F c "); //formato
			gravarArq.printf("-b "); //incluir blob
			gravarArq.printf("-v "); //Verbose - mostra andamento do backup
			gravarArq.printf("-f \"" + Caminho + "\\" + databaseName + ".backup" + "\""); //Arquivo de destino
			gravarArq.printf(" " + databaseName); //banco de dados a fazer backup
			gravarArq.printf("\n"); //banco de dados a fazer backup
			gravarArq.printf("PAUSE"); //banco de dados a fazer backup
			
			arq.close();
			java.awt.Desktop.getDesktop().open(new File(localDoArquivo)); // chamdo o bat
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(0);
	}


}
