package Services;

public class CriaSchema {

	private String adcionaSchemaAise = "";;
	private String adcionaSchemaApice = "";
	private String adcionaSchemaEloArquivo = "";
	private String adcionaSchemaProtocolo = "";
	private String adcionaSchemaSigeloam = "";
	private String adcionaSchemaSigelorj = "";
	private String adcionaSchemaSiscop = "";
	private String adcionaSchemaUnico = "";

	public String getAdcionaSchemaAise() {
		return adcionaSchemaAise;
	}

	public void setAdcionaSchemaAise(String adcionaSchemaAise) {
		this.adcionaSchemaAise = adcionaSchemaAise;
	}

	public String getAdcionaSchemaApice() {
		return adcionaSchemaApice;
	}

	public void setAdcionaSchemaApice(String adcionaSchemaApice) {
		this.adcionaSchemaApice = adcionaSchemaApice;
	}

	public String getAdcionaSchemaEloArquivo() {
		return adcionaSchemaEloArquivo;
	}

	public void setAdcionaSchemaEloArquivo(String adcionaSchemaEloArquivo) {
		this.adcionaSchemaEloArquivo = adcionaSchemaEloArquivo;
	}

	public String getAdcionaSchemaProtocolo() {
		return adcionaSchemaProtocolo;
	}

	public void setAdcionaSchemaProtocolo(String adcionaSchemaProtocolo) {
		this.adcionaSchemaProtocolo = adcionaSchemaProtocolo;
	}

	public String getAdcionaSchemaSigeloam() {
		return adcionaSchemaSigeloam;
	}

	public void setAdcionaSchemaSigeloam(String adcionaSchemaSigeloam) {
		this.adcionaSchemaSigeloam = adcionaSchemaSigeloam;
	}

	public String getAdcionaSchemaSigelorj() {
		return adcionaSchemaSigelorj;
	}

	public void setAdcionaSchemaSigelorj(String adcionaSchemaSigelorj) {
		this.adcionaSchemaSigelorj = adcionaSchemaSigelorj;
	}

	public String getAdcionaSchemaSiscop() {
		return adcionaSchemaSiscop;
	}

	public void setAdcionaSchemaSiscop(String adcionaSchemaSiscop) {
		this.adcionaSchemaSiscop = adcionaSchemaSiscop;
	}
	public String getAdcionaSchemaUnico() {
		return adcionaSchemaUnico;
	}

	public void setAdcionaSchemaUnico(String adcionaSchemaUnico) {
		this.adcionaSchemaUnico = adcionaSchemaUnico;
	}


}
